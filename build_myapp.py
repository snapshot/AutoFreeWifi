﻿#!/usr/bin/python2.5
# -*- coding: utf-8 -*-
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published
## by the Free Software Foundation; version 2 only.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
import py2deb
import os
if __name__ == "__main__":
   try:
      os.chdir(os.path.dirname(sys.argv[0]))
   except:
      pass
   print

   p=py2deb.Py2deb("autofreewifi")   #This is the package name and MUST be in lowercase! (using e.g. "mClock" fails miserably...)
   p.maemodisplayname = "AutoFreeWifi"
   p.description = dict()
   p.description['default']="Automatic connection to FreeWifi network. FreeWifi is a hotspot name of french ISP 'Free'. Using this service requires to be a customer and to have a login and password."
   p.description['fr_FR']="Connexion automatique à un réseau FreeWifi, les hotspot de Free. Nécessite un compte Free et un login/mot de passe."
   p.upgradedescription='Better description of AutoFreeWifi'
   version = "0.6.1"           #Version of your software, e.g. "1.2.0" or "0.8.2"
   build = "7"                 #Build number, e.g. "1" for the first build of this version of your software. Increment for later re-builds of the same version of your software.
   p.author="Gérald Fauvelle"
   p.mail="maemo@fauvelle.net"
   p.depends = "python2.5, python2.5-qt4-gui, python2.5-qt4-core, wireless-tools, wget"
   p.section="user/network"
   p.icon = "src/usr/share/icons/hicolor/40x40/apps/autofreewifi.png"
   p.arch="all"                #should be all for python, any for all arch
   p.urgency="low"             #not used in maemo onl for deb os
   p.distribution="fremantle"
   p.repository="extras-devel"
   p.xsbc_bugtracker="https://projects.forum.nokia.com/AutoFreeWifi/query?status=!closed&order=priority"
   #Set here your post install script
   p.postinstall="""#!/bin/sh
   chmod +x /opt/autofreewifi/config.py
   chmod +x /opt/autofreewifi/autofreewifi
   ln -sf /opt/autofreewifi/autofreewifi /etc/network/if-up.d/autofreewifi
   """
   #run-standalone.sh python /opt/autofreewifi/config.py
   #Set here your post remove script
   p.postremove="""#!/bin/sh
   rm -f /etc/network/if-up.d/autofreewifi
   """
   #  p.preinstall="""#!/bin/sh
   #  chmod +x /usr/bin/mclock.py""" #Set here your pre install script
   #  p.preremove="""#!/bin/sh
   #  chmod +x /usr/bin/mclock.py""" #Set here your pre remove script
   #Text with changelog information to be displayed in the package "Details" tab of the Maemo Application Manager
   changeloginformation = p.upgradedescription
   dir_name = "src"            #Name of the subfolder containing your package source files (e.g. usr\share\icons\hicolor\scalable\myappicon.svg, usr\lib\myapp\somelib.py). We suggest to leave it named src in all projects and will refer to that in the wiki article on maemo.org
   #Thanks to DareTheHair from talk.maemo.org for this snippet that recursively builds the file list
   for root, dirs, files in os.walk(dir_name):
      real_dir = root[len(dir_name):]
      fake_file = []
      for f in files:
         fake_file.append(root + os.sep + f + "|" + f)
      if len(fake_file) > 0:
         p[real_dir] = fake_file
   print p
   r = p.generate(version,build,changelog=changeloginformation,tar=True,dsc=True,changes=True,build=False,src=True)
